/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex10;

/**
 *
 * @author Administrador
 */
public class CarroMotor {

    private Motor motor;

    public CarroMotor(Motor motor) {
        if (motor == null) {
            throw new NullPointerException();
        }
        this.motor = motor;

    }
}

/** Nesse caso, motor está acoplado a classe carro e vice-versa.
**/
