/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex10;

/**
 *
 * @author Administrador
 */

/**a classe ProgramaCoesao tem responsabilidades que não são suas,
  como obter um produto e gravá-lo no banco de dados. Então, 
  dizemos que esta classe não está coesa, ou seja, ela tem responsabilidades demais,
  e o que é pior, responsabilidades que não são suas. 
**/
public class ProgramaCoesao {
    
     public void ExibirFormulario()    {
         //implementação
     }
  
     public void ObterProduto()    {
         //implementação
     }
  
     public void gravarProdutoDB   {
         //implementação
     }
        public void MostrarFormulario()     {
         //Implementação
       }
  
     public void BotaoGravarProduto( ) {
         Produto.gravarProduto();
     }
     
     
     /** uma clara separação de responsabilidades, 
       o que contribui para um design desacoplado e organizado. 
       O formulário não assume o papel de cadastrar o produto, 
       ele pede a quem tem a responsabilidade para que faça tal tarefa.
      **/
     
 
     public class Programa{
         
       public void MostrarFormulario(){
         //Implementação
       }
  
     public void BotaoGravarProduto( ){
         Produto.gravarProduto();
     }
  
 }
     
 }
 
    

